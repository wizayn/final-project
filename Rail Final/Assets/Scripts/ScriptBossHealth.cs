﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScriptBossHealth : MonoBehaviour
{
    public Text levelClear;
    public Text scoreText;
    public GameObject nextLevel;
    public GameObject mainMenu;

    public int waitTime;
    public int bossHealth;



    // Update is called once per frame
    void Update()
    {
        if (bossHealth <= 0)
        {
            StartCoroutine(DisplayScore(waitTime));
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "PlayerBullet")
        {
            bossHealth--;
        }
    }

    IEnumerator DisplayScore(int waitTime)
    {
        levelClear.enabled = true;
        scoreText.enabled = true;
        nextLevel.SetActive(true);
        mainMenu.SetActive(true);
        Time.timeScale = 0.0f;
        yield return new WaitForSeconds(waitTime);
        
    }
}
