﻿using UnityEngine;
using System.Collections;

public class MoveEnemy : MonoBehaviour {

    public float timeToWait = 0.0f;
    public float speed = 1.0f;
    private float startTime;
    private float jL;

    public Transform player;
    public GameObject[] waypoints;

    
	// Use this for initialization
	void Start () {
        StartCoroutine(EnemyMove());
	}
	
	// Update is called once per frame
	void Update () 
    {
        
        transform.LookAt(player);

        
	}

    IEnumerator EnemyMove()
    {
        
        float elapsedTime = 0f;

        yield return new WaitForSeconds(timeToWait);

        foreach (GameObject obj in waypoints)
        {
            Vector3 startPoint = transform.position;
            while (elapsedTime < speed)
            {
                //Debug.Log("OBJ is: " + obj.name);
                transform.position = Vector3.Lerp(startPoint, obj.transform.position, (elapsedTime / speed));
                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            elapsedTime = 0.0f;
        }
    }
}
