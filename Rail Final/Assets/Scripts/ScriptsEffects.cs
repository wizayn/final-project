﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptsEffects
{
    //public EffectTypes type;

    public GameObject endPoint;


    public float duration;
    public float intensity;

    public bool fadeInFromBlack;
    public bool fadeOutToBlack;
    public float fadeInTime;
    public float fadeOutTime;

    public float splatterDuration;
    public bool useSplatterFadeIn;
    public bool useSplatterFadeOut;
    public float splatterFadeInTime;
    public float splatterFadeOutTime;

    public float timeUnderwater;

    public float waitTime;

    public string name;
}
