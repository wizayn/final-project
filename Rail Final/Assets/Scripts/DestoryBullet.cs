﻿using UnityEngine;
using System.Collections;


public class DestoryBullet : MonoBehaviour
{
    
    
    public float destoryTime = 3.0f;
    public int enemyHealth = 5;
    
    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, destoryTime);
    }

    void OnTriggerEnter(Collider col)
    {
        Destroy(gameObject);
    }

    //void OnCollisionEnter(Collision col)
    //{
    //    Destroy(gameObject);
    //}
}
