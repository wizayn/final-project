﻿public enum MovementTypes
{
    WAIT,
    STRAIGHT,
    BEIZIER,
    ROTATEANDRETURN
}

public enum FacingTypes
{
    WAIT,
    LOOKAT,
    FREELOOK,
    FACEDIRECTION
}

public enum BossMovement
{
    WAIT,
    LOOKAT,
    MOVETO
}
//public enum EffectTypes
//{
//    WAIT,
//    SHAKE,
//    SPLATTER,
//    FADE 
//}

