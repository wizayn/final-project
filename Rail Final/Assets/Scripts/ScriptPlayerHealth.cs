﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScriptPlayerHealth : MonoBehaviour {
    public int health;
    public Text gameOver;
  
    public GameObject mainMenu;
	public GameObject heart1;
	public GameObject heart2;
	public GameObject heart3;
	public GameObject heart4;
	public GameObject heart5;

	public GameObject fullHeart1;
	public GameObject fullHeart2;
	public GameObject fullHeart3;
	public GameObject fullHeart4;
	public GameObject fullHeart5;

    public Text endScore;

	ScriptLevelManager levelManager;
    ScriptPlayerShield shielded;

    void Start()
    {
        shielded = GameObject.Find("Capsule").GetComponent<ScriptPlayerShield>();
        
        gameOver.enabled = false;
        mainMenu.SetActive(false);
        endScore.enabled = false;
    }
    
    void Update()
    {
        if (health > 5)
            health = 5;

		if (health == 5) {
			heart1.SetActive (false);
			heart2.SetActive (false);
			heart3.SetActive (false);
			heart4.SetActive (false);
			heart5.SetActive (false);

			fullHeart1.SetActive (true);
			fullHeart2.SetActive (true);
			fullHeart3.SetActive (true);
			fullHeart4.SetActive (true);
			fullHeart5.SetActive (true);
		} else if (health == 4) {
			heart1.SetActive (false);
			heart2.SetActive (false);
			heart3.SetActive (false);
			heart4.SetActive (false);
			heart5.SetActive (true);

			fullHeart1.SetActive (true);
			fullHeart2.SetActive (true);
			fullHeart3.SetActive (true);
			fullHeart4.SetActive (true);
			fullHeart5.SetActive (false);
		} else if (health == 3) {
			heart1.SetActive (false);
			heart2.SetActive (false);
			heart3.SetActive (false);
			heart4.SetActive (true);
			heart5.SetActive (true);

			fullHeart1.SetActive (true);
			fullHeart2.SetActive (true);
			fullHeart3.SetActive (true);
			fullHeart4.SetActive (false);
			fullHeart5.SetActive (false);
		} else if (health == 2) {
			heart1.SetActive (false);
			heart2.SetActive (false);
			heart3.SetActive (true);
			heart4.SetActive (true);
			heart5.SetActive (true);

			fullHeart1.SetActive (true);
			fullHeart2.SetActive (true);
			fullHeart3.SetActive (false);
			fullHeart4.SetActive (false);
			fullHeart5.SetActive (false);
		} else if (health == 1) {
			heart1.SetActive (false);
			heart2.SetActive (true);
			heart3.SetActive (true);
			heart4.SetActive (true);
			heart5.SetActive (true);

			fullHeart1.SetActive (true);
			fullHeart2.SetActive (false);
			fullHeart3.SetActive (false);
			fullHeart4.SetActive (false);
			fullHeart5.SetActive (false);
		} else {
			heart1.SetActive (true);
			heart2.SetActive (true);
			heart3.SetActive (true);
			heart4.SetActive (true);
			heart5.SetActive (true);

			fullHeart1.SetActive (false);
			fullHeart2.SetActive (false);
			fullHeart3.SetActive (false);
			fullHeart4.SetActive (false);
			fullHeart5.SetActive (false);

			//Debug.Log("If health triggered");
			gameOver.enabled = true ;
            endScore.enabled = true;
			mainMenu.SetActive(true);
            Time.timeScale = 0;
		}
    }

	void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "EnemyBullet" && shielded.playerShield == false)
        {
            Debug.Log("Hit by bullet");
            health--;
        }
    }

    public void ReturnToMainMenu()
    {
        SceneManager.LoadScene("Main Menu");
    }
}
