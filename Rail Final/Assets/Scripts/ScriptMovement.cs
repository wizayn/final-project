﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptMovement
{
    public MovementTypes type;

    public GameObject lookTarget;
    public float speed2;

    public GameObject endPoint;
    public float speed;

    public GameObject bezierEndPoint;
    public GameObject bezierCurvePoint;
    public float bezierMoveTime;

    public float waitTime;

    public string name;
}
