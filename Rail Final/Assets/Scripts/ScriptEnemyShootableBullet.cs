﻿using UnityEngine;
using System.Collections;

public class ScriptEnemyShootableBullet : MonoBehaviour {

    ScriptPlayerHealth playerHealth;
    ScriptPlayerShield shielded;
    public int damage;

    // Use this for initialization
    void Start()
    {
        playerHealth = GameObject.Find("Capsule").GetComponent<ScriptPlayerHealth>();
        shielded = GameObject.Find("Capsule").GetComponent<ScriptPlayerShield>();
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "PlayerBullet")
        {
            Debug.Log("Hit Player Bullet");
            Destroy(gameObject);
        }
        else if (col.gameObject.tag == "Player" && shielded.playerShield == false)
        {
            Debug.Log("Hit object " + col.gameObject.name);
            playerHealth.health -= damage;
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
