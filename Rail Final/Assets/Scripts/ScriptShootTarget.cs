﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScriptShootTarget : MonoBehaviour
{
    public Rigidbody bullet;
    public Rigidbody shotGunBullet;

    public Image shotgun;
    public Image nailGun;

    public Text ammoCountText;

    //public GameObject spawnPoint;
    public float speed = 5.0f;
    public float nailGunFireRate = 0.5f;
    private float nextFire = 0.0f;
    public int reloadTime;
    public int ammoMax;
    private int ammoCount;
    
    public int weapon;
    public int shotgunAmmo;
    public int shootGunFireRate;    

    void Start()
    {
        weapon = 1;
        shotgunAmmo = 0;

        shotgun.enabled = false;

        if (ammoMax > 0)
            ammoCount = ammoMax;
        else
            ammoCount = 10;

        if (reloadTime <= 0)
            reloadTime = 1;

        UpdateAmmoCount();
    }

    // Update is called once per frame
    void Update()
    {
        Rigidbody clone;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(reload());
        }

        if (Input.GetMouseButton(0))
        {
                if (weapon == 1 && Time.time > nextFire)
                {
                    if (ammoCount > 0)
                    {
                    nextFire = Time.time + nailGunFireRate;
                    clone = Instantiate(bullet, transform.position, transform.rotation) as Rigidbody;
                    clone.name = "PlayerBullet";
                    clone.velocity = ray.direction.normalized * speed;
                    ammoCount--;
                    UpdateAmmoCount();
                    }
                    else
                    {
                        StartCoroutine(reload());
                    }

                }
                else if (weapon == 2 && Time.time > nextFire)
                {
                    if (shotgunAmmo > 0)
                    {
                        nextFire = Time.time + shootGunFireRate;
                        clone = Instantiate(shotGunBullet, transform.position, transform.rotation) as Rigidbody;
                        clone.name = "PlayerBullet";
                        clone.velocity = ray.direction.normalized * speed;
                        
                        shotgunAmmo--;
                        UpdateShootGunAmmo();
                    }
                    else
                        weapon = 1;
                }
        }
        Debug.DrawRay(ray.origin, ray.direction * 100f, Color.red);

        if (shotgunAmmo > 0)
        {
            shotgun.enabled = true;
        }
        else
            shotgun.enabled = false;
    }

    IEnumerator reload()
    {
        
        yield return new WaitForSeconds(reloadTime);
        UpdateAmmoCount();
        ammoCount = ammoMax;
        //Debug.Log("Ammo Count: " + ammoCount);
        UpdateAmmoCount();

    }

    void UpdateAmmoCount()
    {
        if (weapon == 1)
        {
            ammoCountText.text = "Nailgun Ammo: " + ammoCount;
        }
        else if(weapon == 2)
        {
            ammoCountText.text = "Shotgun Ammo: " + shotgunAmmo;
        }
        //Debug.Log("Update Count");
    }

    void UpdateShootGunAmmo()
    {
        if (weapon == 1)
        {
            ammoCountText.text = "Nailgun Ammo: " + ammoCount;
        }
        else if (weapon == 2)
        {
            ammoCountText.text = "Shotgun Ammo: " + shotgunAmmo;
        }
    }

    public void SetWeaponOne()
    {
        weapon = 1;
    }
    public void SetWeaponTwo()
    {
        weapon = 2;
    }
}
