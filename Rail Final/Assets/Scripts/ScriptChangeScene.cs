﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScriptChangeScene : MonoBehaviour {

    public int waitTime;

    public Text levelClear;
    public Text scoreText;
    public GameObject nextLevel;
    public GameObject mainMenu;
    //public string level;

	public void LoadSceneOne()
    {
        SceneManager.LoadScene("Level 1");
        Time.timeScale = 1.0f;
    }

	public void LoadSceneTwo()
	{
		SceneManager.LoadScene("Level 2");
        Time.timeScale = 1.0f;

	}
    public void LoadCredits()
    {
        SceneManager.LoadScene("Credits");
        Time.timeScale = 1.0f;
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1.0f;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Capsule") 
        {   
            StartCoroutine(DisplayScore(waitTime));
        }     
    }

    IEnumerator DisplayScore(int waitTime)
    {
        levelClear.enabled = true;
        scoreText.enabled = true;
        nextLevel.SetActive(true);
        mainMenu.SetActive(true);
        Time.timeScale = 0.0f;
        yield return new WaitForSeconds(waitTime);        
    }

}
