﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Gizmodos : MonoBehaviour
{
    public List<ScriptMovement> movementTypes;
    public GameObject end;
    Vector3 bCP;
    void OnDrawGizmos()
    {
        movementTypes = GameObject.Find("EngineObject").GetComponent<Engine>().movementList;
        Vector3 startPos = transform.position;
        Gizmos.color = Color.red + Color.magenta + Color.green;
        if (movementTypes.Count != 0)
        {
            for (int i = 0; i < movementTypes.Count; i++)
            {
                switch (movementTypes[i].type)
                {
                case MovementTypes.STRAIGHT:
                    Gizmos.color = Color.green;
                    end = movementTypes[i].endPoint;
                    bCP = end.transform.position;
                    break;
                case MovementTypes.WAIT:
                    Gizmos.color = Color.clear;
                    end = GameObject.Find("EngineObject");
                    bCP = end.transform.position;
                    break;
                case MovementTypes.BEIZIER:
                    Gizmos.color = Color.white;
                    end = movementTypes[i].bezierEndPoint;
                    bCP = end.transform.position;
                    break;
                case MovementTypes.ROTATEANDRETURN:
                    Gizmos.color = Color.blue;
                    end = GameObject.Find("EngineObject");
                    bCP = end.transform.position;
                    break;
                default:
                    Gizmos.color = Color.blue;
                    break;
                }
                
                Gizmos.DrawLine(startPos, bCP);
                startPos = end.transform.position;
            }
        }           
    }

}
