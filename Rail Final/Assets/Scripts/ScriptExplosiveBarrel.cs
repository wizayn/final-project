﻿using UnityEngine;
using System.Collections;

public class ScriptExplosiveBarrel : MonoBehaviour {
    public int explosiveDamage;
    
    GameObject[] enemies;
    float distance;
    public float damageRadios;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "PlayerBullet")
        {
            //Debug.Log("Collision with Player Bullet");
            enemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject obj in enemies)
            {
                //Debug.Log("Enemy is " + obj.name);
                distance = Vector3.Distance(obj.transform.position, this.gameObject.transform.position);
                if (distance < damageRadios && obj.tag == "Enemy")
                {
                    obj.GetComponent<ScriptEnemyHealth>().enemyHealth -= explosiveDamage;
                }
            }
            Destroy(gameObject);
        }
    }
}
