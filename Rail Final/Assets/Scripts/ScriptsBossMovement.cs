﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptsBossMovement 
{
    public BossMovement type;
    
    public Transform waypoint;
    public GameObject lookTarget;
    public GameObject boss;

    public float duration;
    public float moveSpeed;

    public float waitTime;

    public string name;   
}
