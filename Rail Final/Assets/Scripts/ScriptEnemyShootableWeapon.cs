﻿using UnityEngine;
using System.Collections;

public class ScriptEnemyShootableWeapon : MonoBehaviour {
    public Rigidbody shootableObject;
    public Transform player;
    public Transform muzzle;

    public float speed;
    public float delayTime;
    float nextShot = 0.0f;
	
    void Start()
    {
        nextShot = delayTime;
    }
	void Update () 
    {
        transform.LookAt(player);
        
        if (Time.time > nextShot)
        {
            nextShot = Time.time + delayTime;

            Rigidbody b = GameObject.Instantiate(shootableObject, muzzle.position, transform.rotation) as Rigidbody;
            b.name = "ShootableBullet";
            b.AddForce(transform.forward * speed);
            Debug.Log("Created bullet");
        }
	}
}
