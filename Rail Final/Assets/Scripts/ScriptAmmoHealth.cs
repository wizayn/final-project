﻿using UnityEngine;
using System.Collections;

public class ScriptAmmoHealth : MonoBehaviour
{
    [Header("1 = Ammo, 2 = Health")]
  
    public int ammoOrHealth;

    public int healthAmount;
    public int ammoAmount;

    ScriptShootTarget shootGunAmmo;
    ScriptPlayerHealth playerHealth;

    void Start()
    {
        shootGunAmmo = Camera.main.GetComponent<ScriptShootTarget>();
        playerHealth = GameObject.Find("Capsule").GetComponent<ScriptPlayerHealth>();
    }
    // Update is called once per frame

    void OnCollisionEnter(Collision col)
    {
        Debug.Log("Collision with" + col.gameObject.name);
        if (ammoOrHealth == 1)
        {
            if (col.gameObject.name == "PlayerBullet")
            {
                shootGunAmmo.shotgunAmmo += ammoAmount;
                Destroy(gameObject);
            }
        }
        else if (ammoOrHealth == 2)
        {
            if (col.gameObject.name == "PlayerBullet")
            {
                playerHealth.health += healthAmount;
                Destroy(gameObject);
            }
        }
    }
}
