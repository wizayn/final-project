﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScriptPlayerShield : MonoBehaviour
{

    public Image timerBar;
    public bool playerShield = false;
    public float shieldTime = 5.0f;
    public float shieldResetTime;
    public float tempShieldTime;
    bool timerResetting = false;
    ScriptPlayerHealth playerHealth;
    // Use this for initialization
    void Start()
    {
        tempShieldTime = shieldTime;
        playerHealth = GameObject.Find("Capsule").GetComponent<ScriptPlayerHealth>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space) && tempShieldTime > 0.0f && timerResetting == false)
        {
            playerShield = true;
            tempShieldTime -= Time.deltaTime;
            float percentTime = tempShieldTime / shieldTime;
            SetTimerBar(percentTime);
        }
        else
        {
            playerShield = false;
        }

        if((tempShieldTime < 5 && !Input.GetKey(KeyCode.Space)))
        {
            timerResetting = true;
            StartCoroutine(ResetShieldTime(shieldResetTime));
            
        }

        timerBar.fillAmount = tempShieldTime / shieldTime;
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (playerShield == false)
                playerHealth.health--;
            else if (playerShield == true)
            {

            }
        }
    }
    IEnumerator ResetShieldTime(float shieldResetTime)
    {
        yield return new WaitForSeconds(shieldResetTime);
        tempShieldTime = shieldTime;
        timerResetting = false;
        SetTimerBar(tempShieldTime);
    }

    public void SetTimerBar(float tempShieldTime)
    {
        timerBar.GetComponent<Image>().fillAmount = (tempShieldTime / shieldTime);
    }
}
