﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScriptLevelManager : MonoBehaviour {

   

	public Text currentScore;
    public Text endScore;
    public Text gameOver;
    
    public Text levelClear;
	public GameObject nextLevel;
	public GameObject mainMenu;

    public int score;
	// Use this for initialization
	void Start () 
    {
        levelClear.enabled = false;
        endScore.enabled = false;
        gameOver.enabled = false;
		nextLevel.SetActive(false);
		mainMenu.SetActive(false);

        score = 0;
		score = 100;
        UpdateScore();
    }

    public void AddScore(int newScoreValue)
    { 
        score += newScoreValue;
        Debug.Log("AddScore: " +  score +" New Score: " + newScoreValue);
        UpdateScore();
    }

	void UpdateScore () 
    {
        currentScore.text = "Score: " + score;
        endScore.text = "Score: " + score;
	}

    
}
