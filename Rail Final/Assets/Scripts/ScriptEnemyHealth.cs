﻿using UnityEngine;
using System.Collections;

public class ScriptEnemyHealth : MonoBehaviour {
    public int enemyHealth = 5;
    ScriptLevelManager sceneManager;
    public int ScoreValue;

    void Start()
    {
        GameObject engineObj = GameObject.Find("EngineObject");
        sceneManager = engineObj.GetComponent<ScriptLevelManager>();
    }

    void Update()
    {
        if(enemyHealth <= 0)
        {
            sceneManager.AddScore(ScoreValue);
            Destroy(gameObject);
        }
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "PlayerBullet")
        {
            
            enemyHealth--;
        }

        if (enemyHealth <= 0)
        {
            Debug.Log("ScoreValue" + ScoreValue);
            sceneManager.AddScore(ScoreValue);
            Destroy(gameObject);
        }
    }

   
}
