﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScriptFacings
{
    public FacingTypes type;

    public GameObject lookTarget;
    public float speed;

    public float waitTime;

    public float freeLookTime;

    public string name;


}
