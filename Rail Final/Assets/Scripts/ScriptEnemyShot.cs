﻿using UnityEngine;
using System.Collections;

public class ScriptEnemyShot : MonoBehaviour {

    public GameObject player;
    public float viewRange = 10;
    public float rayRange = 5;
    public int speed = 20;
    public float fireRate = 10.0f;
    private float nextFire = 0.0f;
    public RaycastHit lastPos;
    public Vector3 rayDirection = Vector3.zero;

    public Rigidbody bullet;
    public Transform muzzle;

    void Start()
    {
        nextFire = fireRate;
    }
	// Update is called once per frame
	void Update ()
    {
        //Vector3 playerPosition = player.transform.position - transform.position;
              
        if(Time.time > nextFire)
        {
          
            nextFire = Time.time + fireRate;
            Attack();      
        }

        //Debug.DrawRay(gameObject.transform.position, playerPosition, Color.red);
        rayDirection = player.transform.position - transform.position;

        Debug.DrawRay(transform.position, rayDirection.normalized * rayRange, Color.yellow);

    }

    public void Attack()
    {
        rayDirection = player.transform.position - transform.position;
        
        float distance = Vector3.Distance(player.transform.position, transform.position);

        if (distance < 20.0f)
        {
            //Debug.Log("distance < 20");

            if (Physics.Raycast(transform.position, rayDirection.normalized, out lastPos, rayRange))
            {
                //Debug.Log("Rayhit");
                //Debug.Log("Ray Hit the " + LastPos.transform.gameObject.name);
                if (lastPos.collider.tag == "Player")
                {
                    //Debug.Log("Shooting");
                    Rigidbody b = GameObject.Instantiate(bullet, muzzle.position, muzzle.rotation) as Rigidbody;
                    b.name = "EnemyBullet";
                    b.velocity = rayDirection * speed;  
                }
            }
        }
        
    }
}
