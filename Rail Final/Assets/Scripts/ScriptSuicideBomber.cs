﻿using UnityEngine;
using System.Collections;

public class ScriptSuicideBomber : MonoBehaviour
{

    GameObject player;
    public float speed;
    public float viewRange;
    public float rayRange;
    public float explodeRange;
    public int damage;
    public Vector3 rayDirection = Vector3.zero;
    public RaycastHit lastPos;

    ScriptPlayerHealth playerHealth;
    ScriptPlayerShield shielded;

    void Start()
    {
        player = GameObject.Find("Capsule");
        playerHealth = GameObject.Find("Capsule").GetComponent<ScriptPlayerHealth>();
        shielded = GameObject.Find("Capsule").GetComponent<ScriptPlayerShield>();
    }
    // Update is called once per frame
    void Update()
    {
        transform.LookAt(player.transform.position);
        //Vector3 playerPosition = player.transform.position - transform.position;
        //transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed);

        rayDirection = player.transform.position - transform.position;
        float distance = Vector3.Distance(player.transform.position, transform.position);
        //Debug.DrawRay(transform.position, rayDirection.normalized * rayRange, Color.yellow);
        if (Physics.Raycast(transform.position, rayDirection.normalized, out lastPos, rayRange))
        {

            if (distance < rayRange && lastPos.collider.tag == "Player")
            {
                transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed);
            }
        }

        if(distance < explodeRange)
        {
            Explode();
        }
    }

    void Explode()
    {
        if(shielded.playerShield == false)
        {
            playerHealth.health -= damage;
        }
        Destroy(gameObject);
    }
}
